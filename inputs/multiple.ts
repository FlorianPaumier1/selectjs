import Button from "./button";

const SelectMultiple = () => {

    return `<section class='flex flex-lg-column flex-xl-row multi'>
        <label class='label col-xl-3 col-form-label'></label>
        <div class='selectMultiple'>
            <div class='content_select'>
                <div class='content-value'></div>
                <div class='arrow open-dropdown'></div>
            </div>
            <div class='select-list'>
                <input type='text' class='select-filter px-4'>
                    <ul class='py-1 list'></ul>
                </div>
            </div>
        </section>`;
}


export default SelectMultiple
