const Button = (content?: string) => {
    return `<em class='content'>${content}</em><i></i>`
}

export default Button
