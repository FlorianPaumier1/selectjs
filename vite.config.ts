import {defineConfig, splitVendorChunkPlugin} from "vite";

export default defineConfig({
    plugins: [splitVendorChunkPlugin()],
    build: {
        lib: {
            entry: "./Select.ts",
            name: "SelectJs",
        },
        rollupOptions: {
            output: {
                entryFileNames: `assets/[name].min.js`,
            }
        },
        outDir: 'build',
        emptyOutDir: true,
        cssMinify: true,
        sourcemap: false,
        minify: 'terser',
        manifest: true,
        chunkSizeWarningLimit: 500,
        terserOptions: {
            sourceMap: false,
            parse: {
                html5_comments: false
            },
            format: {
                comments: false,
                ecma: 2016
            },
            compress: {
                drop_console: false,
                drop_debugger: false,
                ecma: 2016,
                arguments: true,
                passes: 1
            },
        },
    },
    optimizeDeps: {
        force: true
    }
});
