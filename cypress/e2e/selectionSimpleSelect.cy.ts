import Select from "../../Select";

describe('behavior_select', () => {

    beforeEach(() => {
        cy.visit('http://localhost:5173')
    })

// Tests that the Select class generates a custom select element for a single select target element.
    it("test_select_option", () => {
        // Given
        let target = Cypress.$<HTMLSelectElement>('#single-select').get(0)
        // When
        const select: Select = new Select(target);
        select.generateSelect();

        // Then
        cy.get(".select-container").should("exist");
        cy.get(".selectUnique").should("exist");

        cy.get(".dropdown-child").first().as("firstChild")
        cy.get("@firstChild").click()
        cy.get(".content-value").should("have.text", "Option 1")
    });

// Tests that the Select class generates a custom select element for a single select target element.
    it("test_double_select_option", () => {
        // Given
        let target = Cypress.$<HTMLSelectElement>('#single-select').get(0)
        // When
        const select: Select = new Select(target);
        select.generateSelect();

        // Then
        cy.get(".select-container").should("exist");
        cy.get(".selectUnique").should("exist");

        cy.get(".dropdown-child").first().as("firstChild")
        cy.get("@firstChild").click()
        cy.get(".content-value").should("have.text", "Option 1")


        cy.get(".dropdown-child").eq(1).as("secondChild")
        cy.get("@secondChild").click()
        cy.get(".content-value").should("have.text", "Option 2")
    });

// Tests that the Select class generates a custom select element for a single select target element.
    it("test_filter_option", () => {
        // Given
        let target = Cypress.$<HTMLSelectElement>('#single-select').get(0)
        // When
        const select: Select = new Select(target);
        select.generateSelect();

        // Then
        cy.get(".select-container").should("exist");
        cy.get(".selectUnique").should("exist");

        cy.get(".selectUnique input").type("1")
        cy.get(".dropdown-child").should("have.length", 2)

        cy.get(".dropdown-child").eq(0).should("have.text", "Option 1")
        cy.get(".dropdown-child").eq(1).should("have.text", "Option 10")
    });

// Tests that the Select class generates a custom select element for a single select target element.
    it("test_filter_empty_option", () => {
        // Given
        let target = Cypress.$<HTMLSelectElement>('#single-select').get(0)
        // When
        const select: Select = new Select(target);
        select.generateSelect();

        // Then
        cy.get(".select-container").should("exist");
        cy.get(".selectUnique").should("exist");

        cy.get(".selectUnique input").type("a")
        cy.get(".dropdown-child").should("have.length", 0)
    });

})
