import Select from "../../Select";

describe('generation_select', () => {

    beforeEach(() => {
        cy.visit('http://localhost:5173')
    })

    // Tests that the Select class generates a custom select element for a single select target element.
    it("test_generate_select_single", () => {
        // Given
        let target = Cypress.$<HTMLSelectElement>('#single-select').get(0)
        // When
        const select: Select = new Select(target);
        select.generateSelect();

        // Then
        cy.get(".select-container").should("exist");
        cy.get(".selectUnique").should("exist");
        cy.get(".selectMultiple").should("not.exist");
        cy.get('#single-select').should("not.be.visible");
    });

    // Tests that the Select class generates a custom select element for a multiple select target element.
    it("test_generate_select_multiple", () => {

        // Given
        let target = Cypress.$<HTMLSelectElement>('#multiple-select').get(0)

        // When
        const select = new Select(target);
        select.generateSelect();


        // Then
        cy.get(".select-container").should("exist");
        cy.get(".selectUnique").should("not.exist");
        cy.get(".selectMultiple").should("exist");
        cy.get("#multiple-select").should("not.be.visible");
    });

    // Tests that the Select class does not generate a custom select element if the target element does not have the "select-custom" class.
    it("test_no_select_custom_class", () => {
        // Given
        let target = Cypress.$<HTMLSelectElement>('#no-custom-select').get(0)

        // When
        const select = new Select(target);
        select.generateSelect();

        // Then
        cy.get(".select-container").should("not.exist");
        cy.get(".selectUnique").should("not.exist");
        cy.get(".selectMultiple").should("not.exist");
    });

})
