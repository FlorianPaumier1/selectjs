import Button from "./inputs/button";
import SelectMultiple from "./inputs/multiple";
import SelectSingle from "./inputs/single";

type SelectChild = {
    selected: boolean;
    placeholder: string;
    position: number,
    content: string,
    value: string,
}

class Select {
    private itemList: HTMLDivElement
    private removeItem: any[] = []
    private newSelect = false
    private dropdownList: HTMLUListElement = null;
    private isMultiple: boolean = false;
    private disabled: boolean = false;
    private target: HTMLSelectElement;
    private callbackEvent?: Function;
    private defaultValues: SelectChild[];
    private dropdown: string;
    private content: HTMLElement;
    private buttonOpen: Element;

    constructor(target: HTMLSelectElement, callback?: Function) {
        this.target = target
        this.callbackEvent = callback
        this.isMultiple = target.multiple
        this.defaultValues = [];
        this.dropdown = target.multiple ? ".selectMultiple" : ".selectUnique"
        this.disabled = target.disabled
    }

    select(element: SelectChild): void {
        if (!this.isMultiple) {
            this.target.querySelectorAll("option").forEach((el: HTMLOptionElement) => {
                el.classList.remove("select__item--selected");
            });
        }

        this.target.querySelectorAll("option")[element.position].selected = true;
        this.target.querySelectorAll("option")[element.position].classList.add("select__item--selected");

        if (this.callbackEvent) {
            this.callbackEvent.call(null)
        }
    }

    deselect(element: SelectChild): void {
        this.target.querySelectorAll<HTMLOptionElement>("option")[element.position].selected = false;
        this.target.querySelectorAll<HTMLOptionElement>("option")[element.position].classList.remove("select__item--selected");
    }

    toggleEnable = (): void => {
        this.disabled = !this.disabled
        this.target.disabled = this.disabled
        this.content.classList.toggle("pe-none")
    }

    createElementMulti(element: SelectChild): HTMLLIElement {
        const li: HTMLLIElement = document.createElement("li")
        li.dataset.position = element.position.toString()
        li.innerHTML = Button(element.content)
        li.dataset.value = element.value
        const close = li.querySelector("i")
        close.addEventListener("click", this.deleteElementMulti)
        return li
    }

    handleElementMulti(element: SelectChild): void {
        const li: HTMLLIElement = this.createElementMulti(element)
        const removeLi = this.getDropdownItem(element)
        if (removeLi) {
            removeLi.classList.toggle("remove")

            setTimeout(() => {
                const liPrev: HTMLLIElement = <HTMLLIElement>li.previousSibling;
                const liNext: HTMLLIElement = <HTMLLIElement>li.nextSibling

                if (liPrev !== null) {
                    liPrev.classList.remove('beforeRemove');
                }
                if (liNext !== null) {
                    liNext.classList.remove('afterRemove');
                }

                setTimeout(function () {
                    liPrev?.classList.remove('beforeRemove');
                    liNext?.classList.remove('afterRemove');
                }, 200);
                removeLi.remove()
                this.select(element)
            }, 300);
        }
    }

    getDropdownItem(element: SelectChild): Element {
        return this.content.querySelector(`.dropdown-child[data-position="${element.position}"]`)
    }


    showDropDown = (e?: Event): void => {
        e?.stopPropagation()
        if (this.disabled) return;
        this.content.querySelector(this.dropdown).classList.toggle("open")
    }

    deleteElementMulti = (e: any): void => {
        e.preventDefault()
        const element = e.target.parentElement;

        const li = this.createListItem(this.defaultValues[element.dataset.position])
        const dropdownList = this.content.querySelector("ul.list")
        dropdownList.appendChild(li)
        li.addEventListener("click", () => this.handleElementMulti(this.defaultValues[element.dataset.position]))
        this.deselect(this.defaultValues[element.dataset.position])
        element.remove()
    }

    createListItem(child: SelectChild) {
        const li: HTMLLIElement = document.createElement("li")
        li.classList.add("dropdown-child")
        li.dataset.position = child.position.toString()
        li.innerHTML = child.content
        li.dataset.value = child.value
        return li
    }

    createElementUnique(child: SelectChild): void {
        if (!child.placeholder) {
            let content = this.content.querySelector<HTMLElement>(".content-value")
            content.innerText = this.defaultValues[child.position].content
            content.dataset.value = this.defaultValues[child.position].value
        }
        this.select(child)
    }

    handleElementUnique(ele: SelectChild) {
        this.createElementUnique(ele)
        this.showDropDown()
    }

    generateSelect() {
        const parent = <HTMLElement>this.target.parentNode;

        if (!this.target.classList.contains("select-custom")) return;

        this.target.style.display = "none"
        const label = parent.querySelector("label")
        if (label) label.style.display = "none"

        let placeholder: string = "";

        // @ts-ignore
        this.defaultValues = Array.from(this.target.options).map((child: HTMLOptionElement) => {
            if (child.dataset.placeholder === "true") {
                placeholder = child.innerHTML
            }
            return {
                value: child.value,
                position: child.dataset.position,
                content: child.innerHTML,
                selected: child.selected && child.dataset.placeholder !== "true",
                placeholder: child.dataset.placeholder === "true",
            }
        })

        let container = document.createElement("section")
        container.classList.add("select-container")
        this.newSelect = this.content === undefined

        if (this.isMultiple) {
            if (this.newSelect) {
                container.innerHTML = SelectMultiple()
                this.content = <HTMLElement>container.firstChild
                parent.insertAdjacentElement("afterend", this.content)
                this.dropdownList = this.content.querySelector("ul.list")
            } else {
                this.clear()
                this.dropdownList = this.content.querySelector("ul.list")
            }
            this.itemList = this.content.querySelector(".selectMultiple")
            this.defaultValues.forEach(child => this.generateSelectMulti(child))
        } else {
            if (this.newSelect) {
                container.innerHTML = SelectSingle()
                this.content = <HTMLElement>container.firstChild
                parent.insertAdjacentElement("afterend", this.content)
                this.dropdownList = this.content.querySelector("ul.list")
            } else {
                this.clear()
                this.dropdownList = this.content.querySelector("ul.list")
            }

            this.itemList = this.content.querySelector(".selectUnique")
            this.defaultValues.forEach(child => this.generateSelectUnique(child))
        }

        if (this.newSelect) {
            this.buttonOpen = this.content.querySelector(".open-dropdown")
            this.buttonOpen.addEventListener("click", this.showDropDown.bind(this))
            this.content.querySelector(".selectUnique")?.addEventListener("click", this.showDropDown.bind(this))
            this.content.querySelector(".selectMultiple")?.addEventListener("click", this.showDropDown.bind(this))
            this.content.querySelector<HTMLLabelElement>(".label").innerText = placeholder
            if (!this.isMultiple) {
                this.content.querySelector<HTMLDivElement>(".content-value").innerText = placeholder
            }
            if (this.disabled) {
                this.content.classList.add("pe-none")
            }
        }

        const input = this.content.querySelector(".select-filter")
        input.addEventListener("keyup", this.filterList, true)

        parent.append(container)
        return this
    }

    filterList = (e: any) => {
        e.preventDefault()
        this.clear()
        const value = e.target.value.toLowerCase()

        const filter = this.defaultValues.filter((ele) => {
            return ele.content.toLowerCase().indexOf(value) > -1
        })

        filter.forEach((ele, key) => {
            this.isMultiple ? this.generateSelectMulti(ele) : this.generateSelectUnique(ele)
        })
    }

    clear() {
        const contentSelect = this.content.querySelector<HTMLDivElement>("div.content-value")

        contentSelect.innerHTML = ""
        contentSelect.innerText = ""
        Object.keys(contentSelect.dataset).forEach(item => {
            contentSelect.removeAttribute(`data-${item}`)
        })

        const parent = this.content.querySelector("ul.list")
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

    generateSelectMulti(child: SelectChild) {
        if (child.selected) {
            this.handleElementMulti(child)
        } else if (!child.placeholder) {
            const li = this.createListItem(child)
            this.dropdownList.appendChild(li)
            li.addEventListener("click", () => this.handleElementMulti(child))
        }
    }

    generateSelectUnique(child: SelectChild) {

        if (!child.placeholder) {
            const li = this.createListItem(child)
            this.dropdownList.appendChild(li)
            li.addEventListener("click", () => this.handleElementUnique(child))
        }

        if (child.selected) {
            this.createElementUnique(child)
        }
    }

    getValues() {
        // @ts-ignore
        const values = Array.from(this.target.selectedOptions).map((ele: HTMLOptionElement) => ele.value)
        return this.isMultiple ? values : values.shift();
    }
}

export default Select
